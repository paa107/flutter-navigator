import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Демонстрация навигации',

      // убираем метку режима отладки
      debugShowCheckedModeBanner: false,

      /*
          4. home отображает первичную страницу
          если успользуются маршруты, то вместо home
          нужно использовать initialRoute с нужным маршрутом
        */
      //home: MyHomePage(),
      initialRoute: '/',

      /*
          3. в больших проектах лучше использовать маршруты
          для переходов по страницам
      */
      routes: {
        '/': (BuildContext context) => MyHomePage(),
        '/second': (BuildContext context) => MySecondPage(),
      },
      /*
        6.4. routes определяет только статичные маршруты
        для обработки динамических используется onGenerateRpute
      */
      onGenerateRoute: (routeSettings) {
        var path = routeSettings.name.split('/');

        if (path[1] == 'second') {
          return MaterialPageRoute(
            builder: (context) => MySecondPage(id: path[2]),
            settings: routeSettings,
          );
        }
      },
    );
  }
}

class MyHomePage extends StatelessWidget {
  /*
    9.5. добавим переменную
  */
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      /*
        9.6. Поставить ключ для работы SnackBar
      */
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('Главное окно'),
      ),
      body: Center(
        /*
          1. использование Navigator.push для перехода
        */
        // child: RaisedButton(
        //   onPressed: () {
        //     Navigator.push(context,
        //         MaterialPageRoute(builder: (context) => MySecondPage()));
        //   },
        //   child: Text('Перейти на второе окно'),
        // ),
        /* ********************************************** */
        /*
          5. Для перехода по заданным маршрутам
          используем Navigator.pushNamed
        */
        // child: RaisedButton(
        //   onPressed: () {
        //     Navigator.pushNamed(context, '/second');
        //   },
        //   child: Text('Перейти на второе окно'),
        // ),
        /*
          6. Передаем параметр во второе окно
        */
        child: Column(
          children: [
            RaisedButton(
              onPressed: () {
                Navigator.pushNamed(context, '/second');
              },
              child: Text('Перейти на второе окно'),
            ),
            RaisedButton(
              onPressed: () {
                /*
                  6.1. передаём параметр '123' через маршрут
                  во вторую страницу
                */
                Navigator.pushNamed(context, '/second/123');
              },
              child: Text('Передать параметр "123" на второе окно'),
            ),
            /*
              7.1. создание кнопки открытия модального окна
              через Navigator.push
              "opaque: false" - открывает окно не на весь экран
            */
            RaisedButton(
              // 9.2. для получения возвращаемого значения из диалогового окна нужно сделать функцию async
              onPressed: () async {
                /*
                  9.3. await нужно ставить для получения значения
                  возвращаемое значение передадим в переменную value
                  для дальнейшего использования
                */
                bool value = await Navigator.push(
                  context,
                  PageRouteBuilder(
                    opaque: false,
                    pageBuilder: (BuildContext context, _, __) => MyPopup(),
                    // 8. Анимация появления диалогового окна
                    transitionsBuilder: (__, animation, ___, child) {
                      return FadeTransition(
                        opacity: animation,
                        // Анимация появления
                        //child: child,
                        /*
                          8.2. Комбинация анимации появления и
                          увеличение диалогового окна
                        */
                        child: ScaleTransition(
                          scale: animation,
                          child: child,
                        ),
                      );
                    },
                  ),
                );

                /*
                  9.4. Чтобы использовать полученное значение
                  диалогового окна
                */
                if (value)
                  _scaffoldKey.currentState.showSnackBar(
                    SnackBar(
                        content: Text('Подтверждено!'),
                        backgroundColor: Colors.green),
                  );
                else
                  _scaffoldKey.currentState.showSnackBar(
                    SnackBar(
                        content: Text('Отменено!'),
                        backgroundColor: Colors.red),
                  );
              },
              child: Text('Открыть диалоговое окно'),
            ),
          ],
        ),
      ),
    );
  }
}

/*
  релизация кнопки на втором окна с возвратом на главный экран
  при помощий Navigator.pop
*/
class MySecondPage extends StatelessWidget {
  // 6.2. добавление переменной
  String _id;

  // 6.3 добавление конструктора класса для определения переменной _id
  MySecondPage({String id}) : _id = id;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        /*
          значение переданной переменной выведем в заголовок
        */
        title: Text('Второе окно $_id'),
      ),
      body: Center(
        child: RaisedButton(
          onPressed: () {
            // 2. используем Navigator.pop для возврата
            Navigator.pop(context);
          },
          child: Text('Назад'),
        ),
      ),
    );
  }
}

// 7. модальное окно
class MyPopup extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text('Диалоговое окно'),
      actions: [
        FlatButton(
          onPressed: () {
            // 9.1. параметр после context будет возвращен из окна
            // true
            Navigator.pop(context, true);
          },
          child: Text('OK'),
        ),
        FlatButton(
          onPressed: () {
            // 9.1. параметр после context будет возвращен из окна
            // false
            Navigator.pop(context, false);
          },
          child: Text('Отмена'),
        ),
      ],
    );
  }
}
